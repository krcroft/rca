# -*- coding: utf-8 -*-

"""rca.__main__: executed when the rca directory is called as script."""

from sys import exit
from .rca import main
exit(main())

