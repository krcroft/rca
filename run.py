#!/usr/bin/env python3.7
# -*- coding: utf-8 -*-

"""Convenience wrapper for running RCA directly from source tree."""

from sys import exit
from rca.rca import main

if __name__ == '__main__':
    exit(main())
